#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "lib/graph.h"
#include "lib/fields.h"

// Struct of values defined here
typedef struct
{

} Value;

// Sample compare and print function
int compareStr(Jval left, Jval right)
{
	return strcmp(jval_s(left), jval_s(right));
}

void print(Jval item)
{
	printf("%d", jval_i(item));
}

// Always add vertices FIRST, after that continue to add edges
int main(void)
{
	Graph graph = createGraph();
	int keyToAdd = 0;

	/* Code starts here */

	IS is = new_inputstruct("test.txt");
	if (is == NULL)
	{
		printf("Failed to read file.\n");
		exit(1);
	}

	while (get_line(is) >= 0)
	{
		keyToAdd++;
		int i;
		for (i = 0; i < is->NF; ++i)
		{
			addVertex(&graph, keyToAdd, new_jval_i(keyToAdd));
			float weight = atof(is->fields[i]);
			if (weight > 0)
			{
				addEdgeUndirected(&graph, keyToAdd, i + 1, weight);
			}
		}
	}

	printGraph(graph, print);

	/* Code ends here */

	dropGraph(&graph);
	return 0;
}
