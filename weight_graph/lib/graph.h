#ifndef GRAPH_H_
#define GRAPH_H_

#include "jrb.h"
#include "dllist.h"

#define INFINITIVE_VALUE 10000000
#define MAX_VAL 100

typedef struct
{
	JRB vertices;
	JRB edges;
} Graph;

Graph createGraph(void)
{
	Graph g;

	g.vertices = make_jrb();
	g.edges = make_jrb();

	return g;
}

void resetArray(int *arr)
{
	int i;
	for (i = 0; i < MAX_VAL; ++i)
	{
		arr[i] = 0;
	}
}

void resetToInfinitive(float *arr)
{
	int i;
	for (i = 0; i < MAX_VAL; ++i)
	{
		arr[i] = INFINITIVE_VALUE;
	}
}

JRB getVertex(Graph graph, int key)
{
	return jrb_find_int(graph.vertices, key);
}

void addVertex(Graph *graph, int key, Jval value)
{
	if (getVertex(*graph, key) == NULL)
	{
		jrb_insert_int(graph->vertices, key, value);
	}
}

// In undirected graph, this is equal to adjacent()
float getEdge(Graph graph, int v1, int v2)
{
	JRB node, tree;

	node = jrb_find_int(graph.edges, v1);
	if (node == NULL)
	{
		return INFINITIVE_VALUE;
	}

	tree = (JRB) jval_v(node->val);
	JRB subNode;
	if ((subNode = jrb_find_int(tree, v2)) == NULL)
	{
		return INFINITIVE_VALUE;
	}
	else
		return jval_f(subNode->val);
}

void addEdge(Graph *graph, int v1, int v2, float weight)
{
	JRB node, tree;

	if (getEdge(*graph, v1, v2) == INFINITIVE_VALUE)
	{
		node = jrb_find_int(graph->edges, v1);
		if (node == NULL)
		{
			tree = make_jrb();
			jrb_insert_int(graph->edges, v1, new_jval_v(tree));
		}

		node = jrb_find_int(graph->edges, v1);
		tree = (JRB) jval_v(node->val);
		if (jrb_find_int(tree, v2) == NULL)
		{
			jrb_insert_int(tree, v2, new_jval_f(weight));
		}
	}
}

void addEdgeUndirected(Graph *graph, int v1, int v2, float weight)
{
	addEdge(graph, v1, v2, weight);
	addEdge(graph, v2, v1, weight);
}

void deleteEdge(Graph *graph, int v1, int v2)
{
	JRB node, tree;

	node = jrb_find_int(graph->edges, v1);
	if (node != NULL)
	{
		tree = (JRB) jval_v(node->val);
		JRB subNode = jrb_find_int(tree, v2);
		if (subNode != NULL)
		{
			jrb_delete_node(subNode);
		}
	}
}

// This will also delete all edges that connect to this vertex
void deleteVertex(Graph *graph, int vertex)
{
	JRB node, tree;

	node = jrb_find_int(graph->vertices, vertex);
	if (node != NULL)
	{
		jrb_delete_node(node);
	}

	node = jrb_find_int(graph->edges, vertex);
	if (node != NULL)
	{
		tree = (JRB) jval_v(node->val);
		jrb_free_tree(tree);
		jrb_delete_node(node);
	}

	jrb_traverse(node, graph->edges)
	{
		tree = (JRB) jval_v(node->val);
		JRB subNode;
		subNode = jrb_find_int(tree, vertex);
		if (subNode != NULL)
		{
			jrb_delete_node(subNode);
		}
	}
}

int indegree(Graph graph, int key, int *output)
{
	JRB node;
	int total = 0;

	jrb_traverse(node, graph.edges)
	{
		if (getEdge(graph, jval_i(node->key), key) != INFINITIVE_VALUE)
		{
			output[total++] = jval_i(node->key);
		}
	}

	return total;
}

// In undirected graph, this is equal to getAdjacentVertices()
int outdegree(Graph graph, int key, int *output)
{
	JRB node, tree;
	int total = 0;

	node = jrb_find_int(graph.edges, key);
	if (node == NULL)
	{
		return 0;
	}

	tree = (JRB) jval_v(node->val);
	jrb_traverse(node, tree)
	{
		output[total++] = jval_i(node->key);
	}

	return total;
}

int numOfVertices(Graph graph)
{
	JRB node;
	int total = 0;

	jrb_traverse(node, graph.vertices)
	{
		total++;
	}

	return total;
}

// Result divide to 2 if graph is undirected
int numOfEdges(Graph graph)
{
	JRB node, tree;
	int total = 0;

	jrb_traverse(node, graph.edges)
	{
		tree = (JRB) jval_v(node->val);
		JRB subNode;
		jrb_traverse(subNode, tree)
		{
			total++;
		}
	}

	return total;
}

// Maybe this is applied only in undirected graph
// This can be used to check number of connected components
// NOTE: SET YOUR VISITED ARRAY EQUAL TO 0 BEFORE RUNNING
int BFS(Graph graph, int key, int *visited)
{
	int numNext, listNext[MAX_VAL];
	int totalOfVisitedNodes = 0;
	Dllist node, queue;
//	memset(listNext, 0, sizeof(listNext));
	resetArray(listNext);

	queue = new_dllist();
	dll_append(queue, new_jval_i(key));

	while (!dll_empty(queue))
	{
		node = dll_first(queue);
		int u = jval_i(node->val);
		dll_delete_node(node);

		if (!visited[u])
		{
			visited[u] = 1;
			totalOfVisitedNodes++;
			numNext = outdegree(graph, u, listNext);
			int i;
			for (i = 0; i < numNext; ++i)
			{
				int v = listNext[i];
				if (!visited[v])
				{
					dll_append(queue, new_jval_i(v));
				}
			}
		}
	}

	return totalOfVisitedNodes;
}

// Similar to BFS
int DFS(Graph graph, int key, int *visited)
{
	int numNext, listNext[MAX_VAL];
	int totalOfVisitedNodes = 0;
	Dllist node, stack;
//	memset(listNext, 0, sizeof(listNext));
	resetArray(listNext);

	stack = new_dllist();
	dll_append(stack, new_jval_i(key));

	while (!dll_empty(stack))
	{
		node = dll_last(stack);
		int u = jval_i(node->val);
		dll_delete_node(node);

		if (!visited[u])
		{
			visited[u] = 1;
			totalOfVisitedNodes++;
			numNext = outdegree(graph, u, listNext);
			int i;
			for (i = 0; i < numNext; ++i)
			{
				int v = listNext[i];
				if (!visited[v])
				{
					dll_append(stack, new_jval_i(v));
				}
			}
		}
	}

	return totalOfVisitedNodes;
}

// Apply only in directed graph
// Based on DFS algorithm
// RETURN 0 IF IT FOUNDS A CYCLE
int checkAcylic(Graph graph)
{
	int visited[MAX_VAL];
	int numNext, listNext[MAX_VAL];
	Dllist node, stack;
	JRB vertex;

	stack = new_dllist();
	jrb_traverse(vertex, graph.vertices)
	{
//		memset(visited, 0, sizeof(visited));
		resetArray(visited);
		int start = jval_i(vertex->key);
		dll_append(stack, new_jval_i(start));

		while (!dll_empty(stack))
		{
			node = dll_last(stack);
			int u = jval_i(node->val);
			dll_delete_node(node);

			if (!visited[u])
			{
				visited[u] = 1;
				numNext = outdegree(graph, u, listNext);
				int i;
				for (i = 0; i < numNext; ++i)
				{
					int v = listNext[i];
					if (v == start)
					{
						return 0;
					}
					if (!visited[v])
					{
						dll_append(stack, new_jval_i(v));
					}
				}
			}
		}
	}

	return 1;
}

int topologicalSort(Graph graph, int *output)
{
	if (!checkAcylic(graph))
	{
		printf("This graph contains cycle, cannot perform topoSort.\n");
		return 0;
	}

	Dllist queue, node;
	JRB vertex;
	int numNext, listNext[MAX_VAL];
	int tempIndegree[MAX_VAL];
//	memset(tempIndegree, 0, sizeof(tempIndegree));
	resetArray(tempIndegree);

	queue = new_dllist();
	jrb_traverse(vertex, graph.vertices)
	{
		int start = jval_i(vertex->key);
		tempIndegree[start] = indegree(graph, start, listNext);
		if (tempIndegree[start] == 0)
		{
			dll_append(queue, new_jval_i(start));
		}
	}

	int total = 0;
	while (!dll_empty(queue))
	{
		node = dll_first(queue);
		int v = jval_i(node->val);
		dll_delete_node(node);

		output[total++] = v;
		numNext = outdegree(graph, v, listNext);
		int i;
		for (i = 0; i < numNext; ++i)
		{
			tempIndegree[listNext[i]]--;
			if (tempIndegree[listNext[i]] == 0)
			{
				dll_append(queue, new_jval_i(listNext[i]));
			}
		}
	}

	return total;
}

// Just print a topoSort
void printTopoSort(Graph graph, void (*printFunc)(Jval))
{
	int output[MAX_VAL];
	int n;

	if ((n = topologicalSort(graph, output)) == 0)
	{
		return;
	}

	int i;
	printf("\nTOPOLOGICAL SORT\n");
	for (i = 0; i < n; ++i)
	{
		JRB node = getVertex(graph, output[i]);
		printFunc(node->val);
		printf(" ");
	}

	printf("\n");
}

// Use only in unweighted graph
int getPathByBFS(Graph graph, int start, int stop, int *track)
{
	int numNext, listNext[MAX_VAL], visited[MAX_VAL];
	Dllist node, queue;
//	memset(listNext, 0, sizeof(listNext));
//	memset(visited, 0, sizeof(visited));
	resetArray(listNext);
	resetArray(visited);

	queue = new_dllist();
	dll_append(queue, new_jval_i(start));

	while (!dll_empty(queue))
	{
		node = dll_first(queue);
		int u = jval_i(node->val);
		dll_delete_node(node);

		if (!visited[u])
		{
			visited[u] = 1;
			// If meets stop, there has a way from start to stop
			if (u == stop)
			{
				return 1;
			}
			numNext = outdegree(graph, u, listNext);
			int i;
			for (i = 0; i < numNext; ++i)
			{
				int v = listNext[i];
				if (!visited[v])
				{
					track[v] = u;
					dll_append(queue, new_jval_i(v));
				}
			}
		}
	}

	return 0;
}

// Run BFS on all nodes to find where the connectivity condition doesn't match
// To check weak connected component, get all in&outdegree of checking node
int checkConnectivity(Graph graph)
{
	JRB node;
	int visited[MAX_VAL];

	jrb_traverse(node, graph.vertices)
	{
//		memset(visited, 0, sizeof(visited));
		resetArray(visited);
		int n;
		n = BFS(graph, jval_i(node->key), visited);
		//      printf("%d: %d\n", jval_i(node->key), n);
		if (n < numOfVertices(graph))
		{
			return 0;
		}
	}

	return 1;
}

// Use in undirected graph only. Get all connected components
int getConnectedComponents(Graph graph, int *list)
{
	JRB node;
	int visited[MAX_VAL];
	int total = 0;
//	memset(visited, 0, sizeof(visited));
	resetArray(visited);

	jrb_traverse(node, graph.vertices)
	{
		if (!visited[jval_i(node->key)])
		{
			list[total++] = jval_i(node->key);
			int n = BFS(graph, jval_i(node->key), visited);
			if (n == numOfVertices(graph))
			{
				return 1;
			}
		}
	}

	return total;
}

// Use in undirected graph only. Print all connected components
void printConnectedComponents(Graph graph, void (*printFunc)(Jval))
{
	int list[MAX_VAL];
	int n = getConnectedComponents(graph, list);
	int i;

	for (i = 0; i < n; ++i)
	{
		JRB node = getVertex(graph, list[i]);
		printFunc(node->val);
		printf(": ");
		int visited[MAX_VAL];
//		memset(visited, 0, sizeof(visited));
		resetArray(visited);
		BFS(graph, list[i], visited);

		int j;
		for (j = 0; j < MAX_VAL; ++j)
		{
			if (visited[j])
			{
				//              printf("%d\t", j);
				node = getVertex(graph, j);
				printFunc(node->val);
				printf("\t");
			}
		}
		printf("\n");
	}
}

float shortestPath(Graph graph, int s, int t, int *path, int *length)
{
	float distance[MAX_VAL], min, w, total;
	int previous[MAX_VAL], tmp[MAX_VAL];
	int n, output[MAX_VAL], i, u, v;
	Dllist ptr, queue, node;
	int flags[MAX_VAL];
	resetArray(flags);
	resetToInfinitive(distance);

	distance[s] = 0;
	previous[s] = s;
	flags[s] = 1;
	queue = new_dllist();
	dll_append(queue, new_jval_i(s));
	while (!dll_empty(queue))
	{
		min = INFINITIVE_VALUE;
		dll_traverse(ptr, queue)
		{
			u = jval_i(ptr->val);
			if (min > distance[u])
			{
				min = distance[u];
				node = ptr;
			}
		}
		u = jval_i(node->val);
		flags[u] = 1;
		dll_delete_node(node);
		if (u == t)
		{
			break;
		}

		n = outdegree(graph, u, output);
//		printf("%d\n", n);
		for (i = 0; i < n; ++i)
		{
			v = output[i];
			if (flags[v] == 1)
			{
				continue;
			}
			w = getEdge(graph, u, v);
//			printf("%f\n", w);
			if (distance[v] > distance[u] + w)
			{
				distance[v] = distance[u] + w;
//				printf("%f\n", distance[v]);
				previous[v] = u;
			}
			dll_append(queue, new_jval_i(v));
		}
	}

	total = distance[t];
	if (total != INFINITIVE_VALUE)
	{
		tmp[0] = t;
		n = 1;
		while (t != s)
		{
			t = previous[t];
			tmp[n++] = t;
		}
		for (i = n - 1; i >= 0; --i)
		{
			path[n - i - 1] = tmp[i];
		}
		*length = n;
	}
	return total;
}

int retrieveKey(Graph graph, Jval value, int (*compare)(Jval, Jval))
{
	JRB node;

	jrb_traverse(node, graph.vertices)
	{
		if (compare(node->val, value) == 0)
		{
			return jval_i(node->key);
		}
	}

	return -1;
}

void printGraph(Graph graph, void (*printFunc)(Jval))
{
	printf("\nGRAPH\n");

	JRB node, tree;
	jrb_traverse(node, graph.vertices)
	{
		printf("(%d) as ", jval_i(node->key));
		printFunc(node->val);
		printf(": ");

		JRB keyListEdge = jrb_find_int(graph.edges, jval_i(node->key));
		if (keyListEdge == NULL)
		{
			printf("\n");
			continue;
		}

		tree = (JRB) jval_v(keyListEdge->val);
		JRB subNode;
		jrb_traverse(subNode, tree)
		{
			JRB toBePrinted = getVertex(graph, jval_i(subNode->key));
			printFunc(toBePrinted->val);
			printf("(%.1f)", jval_f(subNode->val));
			printf("\t");
		}

		printf("\n");
	}

	printf("END GRAPH\n");
}

void dropGraph(Graph *graph)
{
	JRB node, tree;

	jrb_free_tree(graph->vertices);
	jrb_traverse(node, graph->edges)
	{
		tree = (JRB) jval_v(node->val);
		jrb_free_tree(tree);
	}

	jrb_free_tree(graph->edges);
	printf("Graph dropped!\n");
}

#endif /* GRAPH_H_ */
