//#ifndef DIRECTED_WEIGHT_GRAPH_H_
//#define DIRECTED_WEIGHT_GRAPH_H_
//
//#include "jrb.h"
//#include "dllist.h"
//
//typedef struct
//{
//	JRB edges;
//	JRB vertices;
//} Graph;
//
//Graph createGraph(void)
//{
//	Graph tempGraph;
//
//	tempGraph.edges = make_jrb();
//	tempGraph.vertices = make_jrb();
//
//	return tempGraph;
//}
//
//void addVertex(Graph *graph, Jval key, Jval value, int (*compare)(Jval, Jval))
//{
//	JRB node;
//
//	node = jrb_find_gen(graph->vertices, key, compare);
//	if (node == NULL)
//	{
//		jrb_insert_gen(graph->vertices, key, value, compare);
//	}
//}
//
//// Retrieve key and value by node->key & node->val
//JRB getVertex(Graph graph, Jval key, int (*compare)(Jval, Jval))
//{
//	JRB node = NULL;
//
//	node = jrb_find_gen(graph.vertices, key, compare);
//	return node;
//}
//
//int numOfVertices(Graph graph)
//{
//	JRB node;
//	int total = 0;
//
//	jrb_traverse(node, graph.vertices)
//	{
//		total++;
//	}
//
//	return total;
//}
//
//int numOfEdges(Graph graph)
//{
//	JRB node, tree;
//	int total = 0;
//
//	jrb_traverse(node, graph.edges)
//	{
//		tree = (JRB) jval_v(node->val);
//		JRB subNode;
//		jrb_traverse(subNode, tree)
//		{
//			total++;
//		}
//	}
//
//	return total;
//}
//
//void printGraph(Graph graph, int (*compare)(Jval, Jval),
//		void (*printFunc)(Jval))
//{
//	printf("GRAPH:\n");
//
//	JRB node, tree;
//	jrb_traverse(node, graph.vertices)
//	{
//		printFunc(node->key);
//		printf(": ");
//		JRB keyListEdge = jrb_find_gen(graph.edges, node->key, compare);
//		if (keyListEdge == NULL)
//		{
//			printf("\n");
//			continue;
//		}
//
//		tree = (JRB) jval_v(keyListEdge->val);
//		JRB subNode;
//		jrb_traverse(subNode, tree)
//		{
//			printFunc(subNode->key);
//			printf("\t");
//		}
//
//		printf("\n");
//	}
//
//	printf("END GRAPH\n");
//}
//
//// Directed graph: this check whether it has a connection from key1 to key2
//float hasEdge(Graph graph, Jval key1, Jval key2, int (*compare)(Jval, Jval))
//{
//	JRB node, tree;
//
//	node = jrb_find_gen(graph.edges, key1, compare);
//	if (node == NULL)
//	{
//		return 0;
//	}
//
//	tree = (JRB) jval_v(node->val);
//	JRB getNodeWithWeight;
//	if ((getNodeWithWeight = jrb_find_gen(tree, key2, compare)) == NULL)
//	{
//		return 0;
//	}
//	else
//		return jval_f(getNodeWithWeight->val);
//}
//
//void addEdge(Graph *graph, Jval key1, Jval key2, float weight,
//		int (*compare)(Jval, Jval))
//{
//	JRB node, tree;
//
//	if (hasEdge(*graph, key1, key2, compare) == 0)
//	{
//		node = jrb_find_gen(graph->edges, key1, compare);
//		if (node == NULL)
//		{
//			tree = make_jrb();
//			jrb_insert_gen(graph->edges, key1, new_jval_v(tree), compare);
//		}
//
//		node = jrb_find_gen(graph->edges, key1, compare);
//		tree = (JRB) jval_v(node->val);
//		if (jrb_find_gen(tree, key2, compare) == NULL)
//		{
//			jrb_insert_gen(tree, key2, new_jval_f(weight), compare);
//		}
//	}
//}
//
//int indegree(Graph graph, Jval key, Jval *output, int (*compare)(Jval, Jval))
//{
//	JRB node;
//	int total = 0;
//
//	jrb_traverse(node, graph.edges)
//	{
//		if (hasEdge(graph, node->key, key, compare) > 0)
//		{
//			output[total++] = node->key;
//		}
//	}
//
//	return total;
//}
//
//int outdegree(Graph graph, Jval key, Jval *output, int (*compare)(Jval, Jval))
//{
//	JRB node, tree;
//	int total = 0;
//
//	node = jrb_find_gen(graph.edges, key, compare);
//	if (node == NULL)
//	{
//		return 0;
//	}
//
//	tree = (JRB) jval_v(node->val);
//	jrb_traverse(node, tree)
//	{
//		output[total++] = node->key;
//	}
//
//	return total;
//}
//
//void dropGraph(Graph *graph)
//{
//	JRB node, tree;
//
//	jrb_free_tree(graph->vertices);
//	jrb_traverse(node, graph->edges)
//	{
//		tree = (JRB) jval_v(node->val);
//		jrb_free_tree(tree);
//	}
//	jrb_free_tree(graph->edges);
//	printf("Graph dropped!\n");
//}
//
//// Worked on cloned graph
//Dllist topologicalSort(Graph *graph, int (*compare)(Jval, Jval))
//{
//	Dllist result = new_dllist();
//
//	while (!jrb_empty(graph->vertices))
//	{
//		JRB node;
//		Jval tempOutput[50];
//		jrb_traverse(node, graph->vertices)
//		{
//			if (indegree(*graph, node->key, tempOutput, compare) == 0)
//			{
//				dll_append(result, node->key);
//				JRB nodeEdge = jrb_find_gen(graph->edges, node->key, compare);
//				if (nodeEdge == NULL)
//				{
//					jrb_delete_node(node);
//					continue;
//				}
//				jrb_delete_node(nodeEdge);
//				jrb_delete_node(node);
//			}
//		}
//	}
//
//	return result;
//}
//
//#endif /* DIRECTED_GRAPH_H_ */
