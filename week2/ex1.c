//Viet chuong trinh chay tham so dong lenh, nhap vao 2 thong so chieu dai -  chieu rong cua mot hinh chu nhat
//roi in ra dien tich, chu vi cua no

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

double area_cal(double height, double width)
{
return height*width;
}

double peri_cal(double height, double width)
{
return 2*(height+width);
}

int main(int argc, char* argv[])
{
    double height,width;
    if(argc!=3)
    {
    printf("Cu phap sai, cu phap dung \"ten_chuong_trinh height width\"");
    return 0;
    }
    printf("\n%s",argv[1]);
    printf("\n%s",argv[2]);
    height = atof(argv[1]);
    width = atof(argv[2]);
    printf("\n%f",height);
    printf("\n%f",width);
    printf("\nChu vi hinh chu nhat la %f",peri_cal(height,width));
    printf("\nDien tich hinh chu nhat la %f",area_cal(height,width));
    return 1;
}
