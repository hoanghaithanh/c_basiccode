/*viet chuong trinh copy noi dung tu file 1 sang file 2*/

#include<stdio.h>

int main(int argc, char* argv[])
{
  FILE *f1,*f2;
  char content[100];
  char ch;
  int i=0,k=0;
  if(argc!=3)
    {
      printf("Sai cu phap, cu phap dung: ten_chuong_tring ten_file_1 ten_file_2\n");
      return 0;
    }
  if((f1=fopen(argv[1],"r"))==NULL)
    {
      printf("Loi doc file %s.\n",argv[1]);
      return 0;
    }
  while((ch=fgetc(f1))!=EOF)
    {
      content[i++] =(char) ch;
      printf("%c",content[i-1]);
    }
  fclose(f1);

  if((f2=fopen(argv[2],"w+"))==NULL)
    {
      printf("Xay ra loi khi tao file %s.\n",argv[2]);
      return 0;
    }
  while (k<i)
   {
     fputc(content[k],f2);
k++;
   }
  fclose(f2);
  return 1;
  
}
