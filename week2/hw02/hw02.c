/*Viet chuong trinh mo phong lenh copy theo cu phap mycp file1 file2 
trong do chuong trinh copy file1 vao file2 hoac tao moi neu file2 chua ton tai truoc do*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"fields.h"
#include"fields.c"

enum {FAIL,SUCCESS};

void help()
{
  printf("Sai ngu phap, cau truc ngu phap de copy tu file 1 sang file 2: mycp file1 file2.\n");
}

int main(int argc, char* argv[])
{
  FILE *f2;
  IS is;
  if((argc!=4) || (strcmp(argv[1],"mycp")!=0)) { help();return 0;}
  
  if((f2=fopen(argv[3],"w+"))==NULL)                                      //Tao file dich
    {
      printf("Xay ra loi khi tao file %s.\n",argv[3]);
      return FAIL;
    }
  else
    {
      if((is = new_inputstruct(argv[2]))==NULL)                           //Doc file nguon, luu vao bien IS is
	{
	  printf("Xay ra loi khi doc file %s.\n",argv[2]);
	  return 0;
	}
      else
	{
	  printf("Denday\n");
	  while(get_line(is)!=-1)
	    {
	      //printf("Denday2\n");
	      fputs(is->text1,f2);                                        //Khi chua doc het cac line cua is thi tiep tuc paste line vao file dich
	    }
	}
      jettison_inputstruct(is);
      fclose(f2);
    }
}
