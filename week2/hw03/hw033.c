/*Viet chuong trinh ma hoa file theo cong sai cho truoc  */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NUM_OF_PAM  3
char encrypt(char ch,int var)
{
  return ch+var;
}

int main(int argc, char* argv[])
{
  FILE *f1,*f2;
  char outputfile[50],chartmp,chara;
  int dolechcong;
  if(argc!=NUM_OF_PAM)
    {
      printf("Sai cu phap, cu phap dung: ten_chuong_trinh ten_file do_lech_cong\n");
      return 0;
    }
  else
    {
      if((f1=fopen(argv[1],"rb"))==NULL)
	{
	  printf("Loi doc file %s.\n",argv[1]);
	}
      else
	{
	  dolechcong = atoi(argv[2]);
	  strcpy(outputfile,"output_");
	  strcat(outputfile,argv[1]);
	  if((f2=fopen(outputfile,"w+"))==NULL)
	    {
	      printf("Loi ghi file dich.\n");
	      return 0;
	    }
	  else
	    {
	     

	       chartmp = fgetc(f1);
	       while(!feof(f1))
		{
			if(chartmp == '\n') {fputc(chartmp,f2);chartmp=fgetc(f1);continue;}
		  chara = encrypt(chartmp,dolechcong);
		if(((int) chara)>122) chara = (char) (((int)chara)%122+96);
		if(((int) chara)<97)) chara = (char) (122+(96-(int)chara)));
		  printf("%c",chara);
		  fputc((int)chara,f2);
		  chartmp=fgetc(f1);
		}
	      fclose(f2);
	    }
	}
    }
      fclose(f1);
}
