#include <stdio.h>
#include<stdlib.h>
#include"lib/SLList.h"
#include"lib/fields.h"
#include<string.h>
int loadFile(LinkedList *list)
{
  IS is;
  int count=0, numofstudent=0;
  char filename[30];
  ElementType value;
  printf("Nhap ten file muon tai: \n");
  scanf("%s",filename);
  is = new_inputstruct(filename);
  if(is==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return 0;
    }
  while(get_line(is)!=-1)
    {
      numofstudent++;
      strcpy(value.name,is->fields[0]);
      strcpy(value.phone,is->fields[1]);
      strcpy(value.email,is->fields[2]);
      insertAfterHead(list,value);
    }
  printf("%d hoc sinh\n",numofstudent);
  jettison_inputstruct(is);
  return numofstudent;
}

int main() {
  int count=0,numofstudent=0,choose=-1;
  LinkedList studentList=NULL;
  LinkedList curr;
  ElementType value;
  while(choose!=6)
    {
      printf("Chon chuc nang: \n");
      printf("1. Doc va nap file\n");
      printf("2. In danh sach\n");
      printf("3. Them mot hoc sinh vao dau danh sach\n");
      printf("4. Them mot hoc sinh vao cuoi danh sach\n");
      printf("5. Giai phong danh sach\n");
      printf("6. Thoat\n");
      scanf("%d",&choose);
      if(choose==1)
	{
  	  numofstudent += loadFile(&studentList);
	}
      if(choose==2)
	{
	  printList(studentList);
	  printf("Co tat ca %d hoc sinh trong danh sach\n",numofstudent);
	}
      if(choose==3)
	{
	  printf("Nhap ten sinh vien muon them: ");
	  scanf("%s",value.name);
	  printf("Nhap so dien thoai: ");
	  scanf("%s",value.phone);
	  printf("Nhap email: ");
	  scanf("%s",value.email);
	  insertAfterHead(&studentList,value);
	  numofstudent++;
	}
      if(choose==4)
	{
	  printf("Nhap ten sinh vien muon them: ");
	  scanf("%s",value.name);
	  printf("Nhap so dien thoai: ");
	  scanf("%s",value.phone);
	  printf("Nhap email: ");
	  scanf("%s",value.email);
	  curr = studentList;
	  while(curr->next!=NULL) curr = curr->next;
	  insertAfter(&curr,value);
	  numofstudent++;
	}
      
      if(choose==5)
	{
	  deleteList(&studentList);
	  numofstudent=0;
	}
    }

   return 0;
}
