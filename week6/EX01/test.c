#include <stdio.h>

typedef struct address_f{
char name[20];
char tel[11];
char email[25];
} address;

struct list_el {
address addr;
struct list_el *next;
}

typedef struct list_el  *node_addr;

node_addr root, curr, prev;

node_addr makeNewNode(char *name, char *tel, char *email)
{
  node_addr noteptr;
  noteptr = (node_addr)malloc(sizeof(*node_addr));
  strcpy((noteptr->addr).name,name);
  strcpy((noteptr->addr).tel,tel);
  strcpy((noteptr->addr).email,email);
  noteptr->next = NULL;
  return noteptr;
}

node_addr makeNodeFromData(address addr)
{
  node_addr noteptr;
  noteptr = (node_addr)malloc(sizeof(*node_addr));
  nodeptr->addr = addr;
  nodeptr->next = NULL;
  return nodeptr;
}

node_addr readNode(node_addr noteptr)
{
  node_addr tmp;
  if(tmp == NULL) return NULL;
  tmp = (node_addr)malloc(sizeof(*node_addr));
  tmp->addr = noteptr->addr;
  tmp->next = noteptr->next;
  printf("%s\t%s\t%s\n",tmp->addr.name,tmp->addr.tel,tmp->addr.email);
}

node_addr readLinkedList(node_addr root)
{
  node_addr tmp = root;
  while(tmp!=NULL)
    {
      readNode(tmp);
      tmp = tmp->next;
    }
}

void freeList(node_addr root)
{
  node_addr tmp=root;
  node_addr curr = tmp;

  while(curr!=NULL)
    {
      curr=tmp->next;
      free(tmp);
    }
  
}

node_addr insertNodeToHead(node_addr root,address data)
{
  node_add tmp;
  tmp = (node_add)malloc(sizeof(*node_add));
  tmp->addr = data;
  tmp->next = root;
  return tmp;
}

int main() {
  int countList=0,count=0;
  address databuff;
   /* my first program in C */
   printf("Hello, World! \n");
   printf("Chon so phan tu muon in: ");
   scanf("%d",&countList);
   for(count=0;count<countList;count++)
     {
       printf("Nhap ten nguoi thu %d",count+1);
       scanf("%[^\n]s",&databuff.name);
       printf("Nhap so dien thoai nguoi thu %d",count+1);
       scanf("%[^\n]s",&databuff.tel);
       printf("Nhap email nguoi thu %d",count+1);
       scanf("%[^\n]s",&databuff.email);
       root=insertNodeToHead(root,databuff);
     }
   readLinkedList(root);
   freeList(root);
   return 0;
}
