#include <stdio.h>
#include<stdlib.h>
#include"lib/SLList.h"
#include"lib/fields.h"
#include<string.h>

void changeDB()
{
  IS is;
  int count=0;
  char filename[20]="";
  FILE *output;
  ElementType value;
  LinkedList list=NULL,curr=NULL;
  printf("Nhap ten file txt: ");
  scanf("%s",filename);
  is = new_inputstruct(filename);
  if(is==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return;
    }
  while(get_line(is)!=-1)
    {
      strcpy(value.name,is->fields[1]);
      for(count=2;is->fields[count][0]!='|';count++)
	{
	  strcat(value.name," ");
	  strcat(value.name,is->fields[count]);
	}
      count++;
      strcpy(value.model,is->fields[count++]);
      strcpy(value.size,is->fields[count++]);
      strcpy(value.price,is->fields[count]);
      insertAfterHead(&list,value);
      strcpy(value.name,"");
    }
  jettison_inputstruct(is);
  output = fopen(strcat(strtok(filename,"."),".dat"),"wb+");
  if(output==NULL)
    {
      printf("Loi ghi file!\n");
      return;
    }
  curr=list;
  while(curr->next!=NULL)
    {
      fwrite(&(curr->data),sizeof(ElementType),1,output);
      curr=curr->next;
    }
  deleteList(&list);
  fclose(output);
}

int readDatFile(LinkedList *list)
{
  int check=0;
  FILE *fin;
  char filename[20];
  ElementType value;
  printf("Nhap file can doc: ");
  scanf("%s",filename);
  fin = fopen(filename,"rb");
  if(fin==NULL)
    {
      printf("Loi doc file!\n");
      return 0;
    }
  while(!feof(fin))
    {
      check = fread(&value,sizeof(ElementType),1,fin);
      if(check==1) insertAfterHead(list,value);
    }
  fclose(fin);
}

int main() {
  int count=0,numofstudent=0,choose=-1;
  LinkedList studentList=NULL;
  LinkedList curr;
  ElementType value;
  while(choose!=6)
    {
      printf("Chon chuc nang: \n");
      printf("0. Chuyen file txt thanh dat\n");
      printf("1. Doc va nap file .dat\n");
      printf("2. In danh sach\n");
      printf("3. Them mot model vao dau danh sach\n");
      printf("4. Them mot model vao cuoi danh sach\n");
      printf("5. Giai phong danh sach\n");
      printf("6. Thoat\n");
      scanf("%d",&choose);
      if(choose==0)
	{
     	 changeDB();
	}
      if(choose==1)
	{
	  readDatFile(&studentList);
	}
      if(choose==2)
	{
	  printList(studentList);
	}
      if(choose==3)
	{

	}
      if(choose==4)
	{

	}
      
      if(choose==5)
	{
	  deleteList(&studentList);
	}
    }

   return 0;
}
