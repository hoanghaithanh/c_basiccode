// ELEMENT DEFINITION
#ifndef SLLIST_H
#define SLLIST_H

typedef struct {
char name[30];
char model[12];
char size[4];
char price[11];
}ElementType;

// SINGLE LINKED LIST DEFINITION

struct Node
{
	ElementType data;
	struct Node *next;
};

typedef struct Node *LinkedList;

// INSERT NODE AFTER THE HEAD

void insertAfterHead(LinkedList *head, ElementType value)
{
	LinkedList tempNode;

	tempNode = (LinkedList)malloc(sizeof(struct Node));
	if (tempNode == NULL)
	{
		printf("Insert failed.\n");
		return;
	}

	tempNode->data = value;
	if (*head == NULL)
	{
		tempNode->next = NULL;
		*head = tempNode;
	}
	else
	{
		tempNode->next = *head;
		*head = tempNode;
	}
}

// INSERT AFTER A DESIGNATED NODE

void insertAfter(LinkedList *cur, ElementType value)
{
	if (*cur == NULL)
	{
		printf("Invalid insert location.\n");
		return;
	}

	LinkedList tempNode;

	tempNode = (LinkedList)malloc(sizeof(struct Node));
	if (tempNode == NULL)
	{
		printf("Insert failed.\n");
		return;
	}

	tempNode->data = value;
	tempNode->next = (*cur)	->next;
	(*cur)->next = tempNode;
}

// INSERT BEFORE A DESIGNATED NODE

void insertBefore(LinkedList *cur, ElementType value)
{
	if (*cur == NULL)
	{
		printf("Invalid insert location.\n");
		return;
	}

	LinkedList tempNode;

	tempNode = (LinkedList)malloc(sizeof(struct Node));
	if (tempNode == NULL)
	{
		printf("Insert failed.\n");
		return;
	}

    // To insert a new node before a designated node, we can assign the 'value' to 'data' of '*cur', and then the new node will
    // store the old data of '*cur'. Add new node function will be the same as insertAfter
	ElementType tempData = (*cur)->data;
	(*cur)->data = value;

	tempNode->data = tempData;
	tempNode->next = (*cur)->next;
	(*cur)->next = tempNode;
}

// DELETE AFTER A DESIGNATED NODE

ElementType deleteNode(LinkedList *head, LinkedList *cur)
{
	LinkedList tempNode;
	ElementType tempValue;

	if (*cur == NULL)
	{
		printf("Invalid delete location.\n");
		return tempValue;
	}

	if (*head == *cur)
	{
		tempValue = (*head)->data;
		*head = (*head)->next;
		free(*cur);
		return tempValue;
	}

	tempValue = (*cur)->data;
	
	// Process to delete the node pointed by '*cur'. We find the node that the 'next' points to '*cur'
	tempNode = *head;
	while ((tempNode != NULL) && (tempNode->next != *cur))
		tempNode = tempNode->next;
	tempNode->next = (*cur)->next;
	free(*cur);

	return tempValue;
}

// TRACK LOCATION. THE FIRST NODE WILL BE NUMBERED AS 1ST

LinkedList trackLocation(LinkedList head, int location)
{
	LinkedList tempNode;
	int i;

	tempNode = head;
	for (i = 1; i < location; ++i)
	{		
		if (tempNode != NULL)
		{
			tempNode = tempNode->next;
		}
	}

	return tempNode;
}

// PRINT LIST

void printList(LinkedList head)
{
	LinkedList tempNode;

	tempNode = head;
	while (tempNode != NULL)
	{
		printf("%s\t%s\t%s\t%s\n", tempNode->data.name,tempNode->data.model,tempNode->data.size,tempNode->data.price);
		tempNode = tempNode->next;
	}
}

// DELETE LIST

void deleteList(LinkedList *head)
{
	LinkedList tempNode;

	tempNode = *head;
	while (*head != NULL)
	{
		*head = (*head)->next;
		free(tempNode);
		tempNode = *head;
	}
}

#endif
