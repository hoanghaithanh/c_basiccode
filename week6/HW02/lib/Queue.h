#ifndef QUEUE_H
#define QUEUE_H

#define MAX 100

// TREE ADT DECLARATION

typedef char LabelType[MAX];

struct node
{
    LabelType label;
    struct node *left;
    struct node *right;
};

typedef struct node *TreeType;

// QUEUE ELEMENT'S DATA DECLARATION

typedef TreeType Element;

// ADT QUEUE

typedef struct
{
    Element queue[MAX];
    int front;
    int rear;
} QueueType;

// INITIALIZE A QUEUE

void initializeQueue(QueueType *q)
{
    q->front = 0;
    q->rear = 0;
}

// RETURN SIZE OF A QUEUE

int getQueueSize(QueueType q)
{
    return (MAX - q.front + q.rear) % MAX;
}

// CHECK WHETHER QUEUE IS EMPTY

int isEmpty(QueueType q)
{
    return (q.front == q.rear);
}

// ENQUEUE

void enqueue(QueueType *q, Element value)
{
    if ( getQueueSize(*q) == (MAX - 1) )
        printf("%s\n", "Queue is full");
    else
    {
        q->queue[q->rear] = value;
        q->rear = (q->rear + 1) % MAX;
    }
}

// DEQUEUE

int dequeue(QueueType *q, Element *temp)
{
    if (isEmpty(*q))
    {
        printf("%s\n", "Queue is empty");
        return 0;
    }
    else
    {
        *temp = q->queue[q->front];
        q->front = (q->front + 1) % MAX;
        return 1;
    }
}
#endif
