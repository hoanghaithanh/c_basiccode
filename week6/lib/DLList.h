// ADT DOUBLY LINKED LIST

// ELEMENT TYPE DEFINITION
#define LEN 100

typedef int ElementType;

// DOUBLY LINKED LIST DEFINITION

struct node
{
	ElementType data;
	struct node *next;
	struct node *prev;
};

typedef struct node* DLList;

// MAKE A NEW NODE 

void insertNode(DLList *head, DLList *tail, ElementType initData)
{
	DLList tempNode;

	// Make new node
	tempNode = (DLList)malloc(sizeof(struct node));
	if(tempNode == NULL)
	{
		printf("%s\n", "Allocation failed");
		return;
	}
	// Need fix
	tempNode->data = initData;
	//
	tempNode->next = NULL;
	tempNode->prev = NULL;

	// Insert node
	if(*head == NULL)
		*head = tempNode;
	else
	{
		(*tail)->next = tempNode;
		tempNode->prev = *tail;
	}

	*tail = tempNode;
}

// PRINT LIST

void printList(DLList head)
{
	DLList tempNode;

	tempNode = head;
	while(tempNode != NULL)
	{
		// Need fix
		printf("%d%-12s%s\n", (tempNode->data).memory, "Mb", (tempNode->data).processID);
		//
		tempNode = tempNode->next;
	}
}

// DELETE LIST

void deleteList(DLList *head, DLList *tail)
{
	DLList tempNode;

	while((*head) != NULL)	
	{
		tempNode = *head;
		*head = (*head)->next;
		free(tempNode);
	}

	*tail = NULL;
}

// FIND NODE BY DATA

DLList trackLocation(DLList head, char findID[])		// Need fix
{
	DLList tempNode;
	
	tempNode = head;
	while ((tempNode != NULL) && (strcmp(findID, (tempNode->data).processID) != 0))		// Need fix
		tempNode = tempNode->next;

	return tempNode;
}

// COUNT NUMBER OF NODES IN LIST

int getNodeNumber(DLList head)
{
	DLList tempNode;
	int count = 0;

	tempNode = head;
	while (tempNode != NULL)
	{
		count++;
		tempNode = tempNode->next;
	}

	return count;
}

// SORT LIST

void sortList(DLList *head, DLList *tail)
{
	DLList ptNode1, ptNode2;

	if ((*head == NULL) || ((*head)->next) == NULL)
		return;
	// Initial value
	ptNode1 = *head;
	ptNode2 = (*head)->next;

	// Simulate the array bubble sort
	while (ptNode1 != NULL)
	{
		// This is similar to j = i + 1 in the array bubble sort's "for" loop
		ptNode2 = ptNode1->next;
		while (ptNode2 != NULL)
		{
			if (ptNode1->data < ptNode2->data)		// Need fix
			{
				// Swap the data
				ElementType tempData = ptNode1->data;
				ptNode1->data = ptNode2->data;
				ptNode2->data = tempData;
			}
			ptNode2 = ptNode2->next;
		}

		ptNode1 = ptNode1->next;
	}

	return;
}

// DELETE A NODE POINTED BY A POINTER

void deleteNode(DLList *head, DLList *cur, DLList *tail)
{
	if ((*cur) == NULL)
	{
		printf("Null, cannot delete.\n");
		return;
	}

	if ( ((*head) == (*cur)) && ((*tail) == (*cur)) )
	{
		*head = NULL;
		*tail = NULL;
		free(*cur);
		*cur = NULL;
		return;
	}

	if ( (*head) == (*cur) )
	{
		*head = (*cur)->next;
		(*cur)->next->prev = (*cur)->prev;
		free(*cur);
		*cur = NULL;
		return;
	}

	if ( (*tail) == (*cur) )
	{
		*tail = (*cur)->prev;
		(*cur)->prev->next = (*cur)->next;
		free(*cur);
		*cur = NULL;
		return;
	}

	// Solve the last case
	(*cur)->prev->next = (*cur)->next;
	(*cur)->next->prev = (*cur)->prev;
	free(*cur);
	*cur = NULL;
	return;
}