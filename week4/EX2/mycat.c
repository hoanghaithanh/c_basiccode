#include<stdio.h>
#include<stdlib.h>

#define FALSE 0
#define SUCCESS 1
#define MAXSIZE 100

int main(int argc, char* argv[])
{
  char buff[MAXSIZE];
  int char_read;
  FILE *fin;

  if(argc!=2)
    {
      printf("Cu phap sai, cu phap dung: tenct ten_file_in\n");
      return FALSE;
    }
  else
    {
      if((fin = fopen(argv[1],"r"))==NULL)
	{
	  printf("Loi doc file dau vao!\n");
	  return FALSE;
	}
    }
  printf("------%s-------\n",argv[1]);
  while(!feof(fin))
    {
      char_read = fread(buff, sizeof(char),MAXSIZE,fin);
      buff[char_read]='\0';
      printf("%s",buff);
    }
  fclose(fin);
}
