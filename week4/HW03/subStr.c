#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#define FALSE 0
#define SUCCESS 1
#define MAXSIZE 81

char* subStr(char *s1,int offset,int number)
{
  char *string;
  int i;
  if(s1==NULL)
    {
      printf("NULL poiter exception!\n");
      return NULL;
    }
  if(offset<0||offset>strlen(s1)-1)
    {
      printf("Offset exceed string's length\n");
      return NULL;
    }
  if(number<1)
  {
    printf("Substring have no character!\n");
    return NULL;
  }
  if(number>(strlen(s1)-offset+1))
    {
      printf("number greater than copyable charater's number!\n");
      number = strlen(s1) - offset + 1;
    }
  string = (char*)malloc(sizeof(char)*(number+1));
  if(string==NULL)
    {
      printf("MEMORY ERROR!\n");
      return NULL;
    }
  for(i=0;i<number;i++)
    {
      string[i] = s1[i+offset];
    }
  string[number]='\0';
  return string;
}

int main()
{
  char string[30];
  char *substring;
  int offset=0;
  int number=0;

  printf("Input string: ");
  scanf("%[^\n]s",string);
  printf("Input Offset: ");
  scanf("%d",&offset);
  printf("Input number of Substring's character: ");
  scanf("%d",&number);
  substring = (char*)malloc(sizeof(char)*(number+1));
  substring=subStr(string,offset,number);
  if(substring!=NULL)
    {
      printf("substring: %s\n",substring);
      free(substring);
      getchar();
      return SUCCESS;
    }
  else
    {
      printf("Error Orcurred!\n");
      free(substring);
      getchar();
      return FALSE;
    }
}
