#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include<time.h>
#define FALSE 0
#define SUCCESS 1
#define MAXSIZE 81

int copyChar(FILE *fin, FILE *fout)
{
  //double time_spent=0;
  //time_t begin;
  //time_t end;

  char buff;
  rewind(fin);
  rewind(fout);
  //begin = clock();
  while(!feof(fin))
    {
	fread(&buff,sizeof(char),1,fin);
	fwrite(&buff,sizeof(char),1,fout);
      //ch=(char)fgetc(fin);
      //fputc((int)ch,fout);
    }
  //end = clock();
  //time_spent = (double) (end-begin)/CLOCKS_PER_SEC;
  //printf("Thoi gian chay thuat toan: %f\n",time_spent);
  return SUCCESS;
}

int copyLine(FILE *fin, FILE *fout)
{
  //double time_spent=0;
  //time_t begin;
  //time_t end;
  rewind(fin);
  rewind(fout);
  char buff[81];
  //begin = clock();
  while(!feof(fin))
    {
      fgets(buff,MAXSIZE,fin);
      fputs(buff,fout);
    }
  //end = clock();
  //time_spent = (double) (end-begin)/CLOCKS_PER_SEC;
  //printf("Thoi gian chay thuat toan: %f\n",time_spent);
  return SUCCESS;
}

int copyBlock(FILE *fin,FILE *fout, int block_num)
{
  //double time_spent=0;
  //time_t begin;
  //time_t end;
  rewind(fin);
  rewind(fout);
  //int block_num=0;
  char buff[MAXSIZE];

  
  //begin = clock();
  while(!feof(fin))
    {
      fread(buff, sizeof(char)*block_num,1,fin);
      fwrite(buff,sizeof(char)*block_num,1,fout);
    }
  //end = clock();
  //time_spent = (double) (end-begin)/CLOCKS_PER_SEC;
  //printf("Thoi gian chay thuat toan: %f\n",time_spent);
  return SUCCESS;
      
}

  
int main(int argc, char* argv[])
{
  int choose=-1, block=0;
  char c;
  double time_spent = 0;
  time_t begin = 0;
  time_t end = 0;
  FILE *fin, *fout;
  if(argc!=3)
    {
      printf("Loi cu phap, cu phap dung: tenct ten_file_nguon ten_file_dich\n");
      return FALSE;
    }
  if((fin=fopen(argv[1],"r"))==NULL)
    {
      printf("Loi doc file dau vao!");
      return FALSE;
    }
  if((fout=fopen(argv[2],"w+"))==NULL)
    {
      printf("Loi ghi file dau ra!");
      return FALSE;
    }
  while(choose!=4)
    {
      printf("Chon chuc nang:\n1.Copy tung ky tu\n2.Copy theo dong\n3.Copy theo khoi\n4.Exit\n");
	scanf("%d",&choose);
     if(choose==1) 
	{
	begin = clock();
	copyChar(fin,fout);
	end = clock();
	time_spent = (double)(end-begin)/CLOCKS_PER_SEC;
	printf("Thoi gian thuc hien: %f s\n",time_spent);
	}
      if (choose==2)
	{
	begin = clock();
	copyLine(fin,fout);
	end = clock();
	time_spent = (double)(end-begin)/CLOCKS_PER_SEC;
	printf("Thoi gian thuc hien: %f s\n",time_spent);
	}
      if(choose==3)
	{
	printf("Chon so chu cai trong mot block: ");
        scanf("%d",&block);
	begin = clock();
	copyBlock(fin,fout,block);
	end = clock();
	time_spent = (double)(end-begin)/CLOCKS_PER_SEC;
	printf("Thoi gian thuc hien: %f s\n",time_spent);
	}
    }
  fclose(fin);fclose(fout);
  return SUCCESS;
}
