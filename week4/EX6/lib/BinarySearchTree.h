#include "Queue.h"
#define TRUE 1
#define FALSE 0

// ACCESS LEFT AND RIGHT CHILD

TreeType toLeftChild(TreeType treeNode)
{
	if (treeNode != NULL)
		return treeNode->left;
	else return NULL;
}

TreeType toRightChild(TreeType treeNode)
{
	if (treeNode != NULL)
		return treeNode->right;
	else return NULL;
}

// CHECK WHETHER A TREE NODE IS A LEAF

int isLeaf(TreeType treeNode)
{
	if (treeNode != NULL)
		return ((treeNode->left == NULL) && (treeNode->right == NULL));
	else return 0;
}

// CALCULATE THE NUMBER OF TREE NODES

int getTreeNodeNumber(TreeType treeNode)
{
	if (treeNode == NULL)
		return 0;
	else return ( 1 + getTreeNodeNumber(treeNode->left) + getTreeNodeNumber(treeNode->right) );
}

// CALCULATE THE DEPTH OF TREE

int getTreeDepth(TreeType treeNode)
{
	if (treeNode == NULL)
		return 0;
	int ld = getTreeDepth(treeNode->left);
	int rd = getTreeDepth(treeNode->right);
	return 1 + (ld > rd ? ld : rd);
}

// CALCULATE THE NUMBER OF LEAFS

int getTreeLeafNumber(TreeType treeNode)
{
	if (treeNode == NULL)
		return 0;
	if (isLeaf(treeNode))
		return 1;
	else return getTreeLeafNumber(treeNode->left) + getTreeLeafNumber(treeNode->right);
}

// CALCULATE THE NUMBER OF INTERNAL NODES

int getInternalNodeNumber(TreeType treeNode)
{
	if (treeNode == NULL)
		return 0;
	if (isLeaf(treeNode))
		return 0;
	else return ( 1 + getInternalNodeNumber(treeNode->left) + getInternalNodeNumber(treeNode->right) );
}

// CALCULATE THE NUMBER OF RIGHT CHILDS

int getRightChildNumber(TreeType treeNode)
{
	if (treeNode == NULL)
		return 0;
	int countRight = (treeNode->right != NULL) ? 1 : 0;
	return countRight + getRightChildNumber(treeNode->right) + getRightChildNumber(treeNode->left);

}

// MAKE NEW TREE NODE
// NEED TO FIX THE NUMBER OF REFERENCES

TreeType insertBinaryTreeNode(TreeType treeNode, LabelType value)
{
	TreeType tempNode;

	if (treeNode == NULL)
	{
		tempNode = (TreeType)malloc(sizeof(struct node));
		if (tempNode == NULL)
		{
			printf("%s\n", "Out of memory");
			return NULL;
		}

		// Need fix
		strcpy(tempNode->label, value);
		//
		tempNode->left = NULL;
		tempNode->right = NULL;

		return tempNode;
	}
	else if (strcmp(treeNode->label, value) < 0)		// Need fix
		treeNode->right = insertBinaryTreeNode(treeNode->right, value);
	else 
		treeNode->left = insertBinaryTreeNode(treeNode->left, value);

	return treeNode;
}

// MAKE AN ELEMENT VALUE BECOME A TREE (NODE)
// REMEMBER TO FIX THE METHOD OF COPYING DATA

TreeType makeTree(LabelType value)
{
	TreeType tempNode;

	// Remember that we must allocate the size of STRUCT to malloc, not size of POINTER
	// Therefore it has to be node, not TreeType
	tempNode = (TreeType)malloc(sizeof(struct node));
	if (tempNode == NULL)
	{
		printf("Out of memory.\n");
		return NULL;
	}

	// Need fix
	strcpy(tempNode->label, value);
	//
	tempNode->left = NULL;
	tempNode->right = NULL;

	return tempNode;
}

// CREATE A TREE FROM TWO SUBTREE

TreeType mergeTree(TreeType tree1, TreeType tree2, LabelType value)
{
	TreeType tempNode;

	// Remember that we must allocate the size of STRUCT to malloc, not size of POINTER
	// Therefore it has to be node, not TreeType
	tempNode = (TreeType)malloc(sizeof(struct node));
	if (tempNode == NULL)
	{
		printf("Out of memory.\n");
		return NULL;
	}

	// Need fix
	strcpy(tempNode->label, value);
	//
	tempNode->left = tree1;
	tempNode->right = tree2;

	return tempNode;	
}

// DFS INORDER TRAVERSAL
// WE NEED TO CHANGE THE PRIMITIVE VALUE OF label

void inOrderTraverse(TreeType treeNode)
{
	if (treeNode != NULL)
	{
		inOrderTraverse(treeNode->left);
		// Need fix
		printf("%s\t", treeNode->label);
		//
		inOrderTraverse(treeNode->right);
	}
}

// BFS
// WE NEED TO CHANGE THE PRIMITIVE VALUE OF label

void treeBFS(TreeType treeNode)
{
	QueueType q;
	initializeQueue(&q);

	if (treeNode != NULL)
	{
		int temp = 1;
		enqueue(&q, treeNode);
		while ( !isEmpty(q) )
		{
			int times = 0;
			int count = temp;
			temp = 0;
			while (times < count)
			{
				treeNode = dequeue(&q);
				// Need fix
				printf("%-13s", treeNode->label);
				//

				times++;

				if (treeNode->left != NULL)
				{
					enqueue(&q, treeNode->left);
					temp++;
				}

				if (treeNode->right != NULL)
				{
					enqueue(&q, treeNode->right);
					temp++;
				}
			}

			printf("\n");
		}
	}
}

// SEARCH A NODE
// WE MUST CHANGE THE COMPARISON METHOD IN THE IF/ELSE FUNCTION

TreeType searchNode(TreeType treeNode, LabelType value)
{
	if (treeNode == NULL)
		return NULL;
	else if (strcmp(treeNode->label, value) == 0)		// Need fix
		return treeNode;
	else if (strcmp(treeNode->label, value) > 0)		// Need fix
		return searchNode(treeNode->left, value);
	else return searchNode(treeNode->right, value);
}

// FIND MIN VALUE OF A TREE/SUBTREE

TreeType getTreeMinValue(TreeType treeNode)
{
	if (treeNode == NULL)
		return NULL;
	else if (treeNode->left == NULL)
		return treeNode;
	else return getTreeMinValue(treeNode->left);
}

// DELETE A NODE
// NEED TO FIX THE COMPARISON METHOD AND ASSIGN METHOD

TreeType deleteNode(TreeType treeNode, LabelType value)
{
	TreeType tempNode;
	LabelType tempValue;

	if (treeNode == NULL)
		printf("%s\n", "Value not found.");
	else if (strcmp(treeNode->label, value) < 0)		// Need fix
		treeNode->right = deleteNode(treeNode->right, value);
	else if (strcmp(treeNode->label, value) > 0)		// Need fix
		treeNode->left = deleteNode(treeNode->left, value);
	// Find node to delete
	else
	{
		// Node has 2 subtrees
		if (treeNode->left && treeNode->right)
		{
			// The replaced node will be the leftmost child of the right subtree
			tempNode = getTreeMinValue(treeNode->right);
			// Need fix
			strcpy(tempValue, tempNode->label);
			strcpy(treeNode->label, tempValue);
			//
			treeNode->right = deleteNode(treeNode->right, tempValue);
		}
		// Node has 1 or 0 subtree
		else
		{
			tempNode = treeNode;
			if (treeNode->left == NULL)
				treeNode = treeNode->right;
			else if (treeNode->right == NULL)
				treeNode = treeNode->left;
			free(tempNode);
		}
	}

	return treeNode;
}

// DELETE TREE

void deleteTree(TreeType *root)
{
	if (*root == NULL)
		return;
	if ((*root)->left != NULL)
		deleteTree(&(*root)->left);
	if ((*root)->right != NULL)
		deleteTree(&(*root)->right);
	free(*root);
	*root = NULL;

}

