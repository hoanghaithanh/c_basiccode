#include<stdio.h>
#include<stlib.h>
#include"lib/fields.h"


typedef struct {
  char name[30];
  char MSSV[8];
  char SDT[11];
  float diem;
} sinhvien;


int main(int argc, char* argv[])
{
  IS is;
  FILE *fout;
  int i=0, numofstudent = 0;
  sinhvien *student_list;
  if(argc!=3)
    {
      printf("Loi cu phap, cu phap dung: tenct tenFileIn tenFileOut\n");
      return 0;
    }

  else
    {
      if((is=new_inputstruct(argv[1]))==NULL)
	{
	  printf("Loi doc file dau vao!\n");
	  return 0;
	}
      get_line(is);
      
      /*bat dau nhap file vao mang*/
      while(get_line(is)!=-1)
	{
	  numofstudent++;
	  student_list = (sinhvien*) malloc (sizeof(sinhvien)*numofstudent);
	  strcpy(student_list[numofstudent-1].name, is->fields[0]);
	  strcpy(student_list[numofstudent-1].MSSV, is->fields[1]);
	  strcpy(student_list[numofstudent-1].SDT, is->fields[2]);
	  student_list[numofstudent-1].diem = atof(is->fields[3]);
	}

      for(i=0;i<numofstudent;i++)
	{
	  printf("%s %s %s %f\n",student_list[i].name,student_list[i].MSSV,student_list[i].SDT,student_list[i].diem);
    }
      jettison_inputstruct(is);
}
