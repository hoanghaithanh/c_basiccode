#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include"lib/fields.h"
#include<time.h>
#define FALSE 0
#define SUCCESS 1
#define MAXSIZE 81

typedef struct{
  int size;
  char name[20];
  char day[11];
}header;



int main(int argc, char* argv[])
{
  header hd;
  FILE *fin;
  if(argc!=2)
    {
      printf("Loi cu phap! Cu phap dung: tenct tenfile\n");
      return FALSE;
    }
  fin = fopen(argv[1],"rb");
  if(fin==NULL)
    {
      printf("Loi doc file dau vao!\n");
      //fclose(fin);
      return FALSE;
    }
  if(fread(&hd,sizeof(header),1,fin)!=1)
    {
      printf("File khong co header!\n");
      fclose(fin);
      return FALSE;
    }
  else
    {
      if(strcmp(hd.name,"hoang hai thanh")!=0)
	{
	  printf("File khong co header!\n");
	  fclose(fin);
	  return FALSE;
	}
      else
	{
	  printf("------HEADER------\n%d\n%s\n%s\n",hd.size,hd.name,hd.day);
	}
    }
  fclose(fin);
}
