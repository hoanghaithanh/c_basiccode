#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include"lib/fields.h"
#include"lib/fields.c"
#define FALSE 0
#define SUCCESS 1
#define MAXSIZE 81

/*Cau truc thong tin sinh vien*/

typedef struct {
  char name[30];
  char MSSV[9];
  char SDT[11];
  float diem;
}studentStruct;

/*Ham in thong tin 1 sinh vien*/

void printStudent(studentStruct student)
{
  printf("%s\t%s\t%s\t%f\n",student.name,student.MSSV,student.SDT,student.diem);
}

/*Ham in thong tin 1 list sinh vien*/

void printStudentList(studentStruct *list, int number)
{
  int i=0;
  for(i=0;i<number;i++)
    {
      printStudent(list[i]);
    }
}


int main(int argc, char* argv[])
{
  studentStruct *studentList,student;
  IS is;
  FILE *fout;
  int studentnum=0, count=0;
  
  /*Read file dau ra cua tuan truoc*/
  
  if(argc!=2)//Kiem tra tham so dau vao
    {
      printf("Sai cu phap, cu phap dung: tenct ten_file_bangdiem\n");
      return FALSE;
    }
  
  is = new_inputstruct(argv[1]);
  
  if(is==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return FALSE;
    }
  
  get_line(is);
  studentList = (studentStruct*)malloc(sizeof(studentStruct)*studentnum); //cap phat bo nho cho mang studentList
  if(studentList==NULL)
    {
      printf("MEMORICAL ERROR!\n");
      return FALSE;
    }
  
  while(get_line(is)!=-1)
    {
      studentnum++;
      studentList = (studentStruct*)realloc(studentList,sizeof(studentStruct)*studentnum);
      if(studentList==NULL)
	{
	  printf("MEMORICAL ERROR!\n");
	  return FALSE;
	}
      strcpy(studentList[studentnum-1].name,is->fields[1]);
      strcpy(studentList[studentnum-1].MSSV,is->fields[2]);
      strcpy(studentList[studentnum-1].SDT,is->fields[3]);
      studentList[studentnum-1].diem = atof(is->fields[4]);
      // printStudent(studentList[studentnum-1]);
    }
  //printStudentList(studentList,studentnum);

  
  /*Ghi du lieu ra file grade.dat*/
  
  if((fout=fopen("grade.dat","wb+"))==NULL)
    {
      printf("Loi ghi file output!\n");
      return FALSE;
    }

  for(count=0;count<studentnum;count++)
    {
      fwrite(&studentList[count],sizeof(studentStruct),1,fout);
    }
  
  fclose(fout);
  
  /*Doc du lieu tu file grade.dat*/
  
  fout=fopen("grade.dat","rb");
  if(fout==NULL)
    {
      printf("loi doc file grade.txt!\n");
      return FALSE;
    }
  
  while(!feof(fout))
    {
      if(fread(&student,sizeof(studentStruct),1,fout)==1)
      printStudent(student);
    }
  /*Giai phong bo nho, file*/
  fclose(fout);
  jettison_inputstruct(is);
  free(studentList);
  return SUCCESS;
}
