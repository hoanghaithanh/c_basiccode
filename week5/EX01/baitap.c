#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#define FALSE 0
#define SUCCESS 1
#define MAXSIZE 81

typedef struct {
  char name[30];
  char MSSV[9];
  char SDT[11];
  float diem;
}studentStruct;

void printStudent(studentStruct student)
{
  printf("%s\t%s\t%s\t%f\n",student.name,student.MSSV,student.SDT,student.diem);
}

int main(int argc,char* argv[])
{
  int from=0,to=0,count=0;
  studentStruct *list;
  FILE *fin;
  /*Kiem tra dau vao*/
  if(argc!=4)
    {
      printf("Loi cu phap, cu phap dung: ten_ct ten_file num_from num_end\n");
      return FALSE;
    }

  if((fin=fopen(argv[1],"r+"))==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return FALSE;
    }

  from = atoi(argv[2]);
  to = atoi(argv[3]);
  if(from>to)
    {
      printf("Invalid argument!\n");
      return FALSE;
    }

  /*Doc file va ghi vao mang dong*/
  if(fseek(fin, (from-1)*sizeof(studentStruct),SEEK_SET)!=0)
    {
      printf("Vuot qua gioi han file!\n");
      return FALSE;
    }
  
   list = (studentStruct*)malloc(sizeof(studentStruct)*(to-from+1));
     if(list==NULL)
    {
      printf("MEMORICAL ERROR!\n");
      return FALSE;
    }
  fread(list,sizeof(studentStruct),to - from +1,fin);

  for(count=0;count<=to-from;count++)
    {
      printStudent(list[count]);
    }
  
  /*Thay doi du lieu*/
  for(count=0;count<=to-from;count++)
    {
      printf("Nhap lai diem sinh vien thu %d: ",count+1);
      scanf("%f",&(list[count].diem));
    }
  if(fseek(fin, (from-1)*sizeof(studentStruct),SEEK_SET)!=0)
    {
      printf("Vuot qua gioi han file!\n");
      return FALSE;
    }
  fwrite(list,sizeof(studentStruct),to-from+1,fin);
  fclose(fin);
  free(list);
  return SUCCESS;
}
