#include<stdio.h>
#include<stdlib.h>
#include <string.h>
#include"lib/fields.h"
#define FALSE 0
#define SUCCESS 1
#define MAXSIZE 81

typedef struct{
  char name[20];
  char SDT[12];
}contact;

void printContact(contact ct)
{
  printf("%s\t%s\n",ct.name,ct.SDT);
}

void printContactList(contact *list, int numoflist)
{
  int count=0;
  for(count=0;count<numoflist;count++)
    {
      printContact(list[count]);
    }
}

void printContactFile(char *filename)
{
  FILE *fin;
  int count=0, contactCount=0;
  contact *contactList;

   fin = fopen(filename,"rb");
  if(fin==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return ;
    }
  contactList = (contact*)malloc(sizeof(contact));
  if(contactList==NULL)
    {
      printf("MEMORICAL ERROR!\n");
      return;
    }
  while(!feof(fin))
    {
      contactCount++;
      contactList = (contact*)realloc(contactList,sizeof(contact)*contactCount);
      if(fread(&(contactList[contactCount-1]),sizeof(contact),1,fin)!=1)
	{
	  contactCount--;
        contactList = (contact*)realloc(contactList,sizeof(contact)*contactCount);
	}
    }
  printContactList(contactList, contactCount);
  fclose(fin);
  free(contactList);
}

int filesplit(char *filenamein,int numofcontact, char* filenameout1, char* filenameout2)
{
  FILE *fin, *fout1, *fout2;
  int count=0, contactCount=0;
  contact *contactList;
  fin = fopen(filenamein,"rb");
  if(fin==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return FALSE;
    }

  contactList = (contact*)malloc(sizeof(contact));
  while(!feof(fin))
    {
      contactCount++;
      contactList = (contact*)realloc(contactList,sizeof(contact)*contactCount);
      if(fread(&(contactList[contactCount-1]),sizeof(contact),1,fin)!=1)
	{
	contactCount--;
        contactList = (contact*)realloc(contactList,sizeof(contact)*contactCount);
	}
    }
  printContactList(contactList, contactCount);
  fclose(fin);
  fout1 = fopen(filenameout1,"wb+");
  if(fout1==NULL)
    {
      printf("Loi ghi file!\n");
      return FALSE;
    }
  for(count=0;count<numofcontact;count++)
    {
      fwrite(&(contactList[count]),sizeof(contact),1,fout1);
    }
  fclose(fout1);
  fout2 = fopen(filenameout2,"wb+");
  if(fout2==NULL)
    {
      printf("Loi ghi file!\n");
      return FALSE;
    }
  for(count=numofcontact;count<contactCount;count++)
    {
      fwrite(&(contactList[count]),sizeof(contact),1,fout2);
    }
  fclose(fout2);
  printf("---------File1---------\n");
  printContactFile(filenameout1);
   printf("---------File2---------\n");
  printContactFile(filenameout2);
  free(contactList);
  return SUCCESS;
}

int changeDataBase(char *filename)
{
  FILE *fout;
  contact *contactList;
  int count=0, contactCount=0;
  IS is;

  char *newfilename;

  is = new_inputstruct(filename);
  if(is==NULL)
    {
      printf("Loi doc file text database!\n");
      return FALSE;
    }
  contactList = (contact*)malloc(sizeof(contact));
  while(get_line(is)==2)
    {
      contactCount++;
      contactList = (contact*)realloc(contactList,contactCount*sizeof(contact));
      strcpy(contactList[contactCount-1].name,is->fields[0]);
      strcpy(contactList[contactCount-1].SDT,is->fields[1]);
      // printContact(contactList[contactCount-1]);
    }
  // printContactList(contactList,contactCount);
  newfilename = strtok(filename,".");
 
  strcat(newfilename,".dat");
   printf("%s\n",newfilename);
  fout = fopen(newfilename,"wb");
    if(fout==NULL)
      {
	printf("Loi ghi file dau ra!\n");
	return FALSE;
      }
  for(count = 0;count<contactCount;count++)
    {
      fwrite(&(contactList[count]),sizeof(contact),1,fout);
    }
  fclose(fout);
  printContactFile(newfilename);
  //fclose(fout);
  jettison_inputstruct(is);
  // fclose(fout);
}

int filemerge(char *filein1, char *filein2, char *fileout)
{
  int count=0, contactCount = 0;
  FILE *fin1, *fin2, *fout;
  contact *contactList;
  fin1 = fopen(filein1,"rb");
  if(fin1==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return FALSE;
    }
  contactList = (contact*)malloc(sizeof(contact));
  if(contactList==NULL)
    {
      printf("MEMORICAL ERROR!\n");
      return FALSE;
    }
  while(!feof(fin1))
    {
      contactCount++;
      contactList = (contact*)realloc(contactList,sizeof(contact)*contactCount);
      if(fread(&(contactList[contactCount-1]),sizeof(contact),1,fin1)!=1)
	{
	contactCount--;
        contactList = (contact*)realloc(contactList,sizeof(contact)*contactCount);
	}
    }
  fclose(fin1);
  /*Read file input 2*/
  fin2 = fopen(filein2,"rb");
  if(fin2==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return FALSE;
    }

  while(!feof(fin2))
    {
      contactCount++;
      contactList = (contact*)realloc(contactList,sizeof(contact)*contactCount);
    if(fread(&(contactList[contactCount-1]),sizeof(contact),1,fin2)!=1)
	{
	contactCount--;
        contactList = (contact*)realloc(contactList,sizeof(contact)*contactCount);
	}
    }
  fclose(fin2);
  fout=fopen(fileout,"wb+");
  if(fout==NULL)
    {
      printf("Loi ghi file!\n");
      return FALSE;
    }
  for(count=0;count<contactCount;count++)
	{
	  fwrite(&(contactList[count]),sizeof(contact),1,fout);
	}
  printContactList(contactList,contactCount);
  printContactFile(fileout);
  fclose(fout);
  //printContactList(contactList);
  free(contactList);
  return SUCCESS;
}

int main(int argc, char* argv[])
{
  int count=0;
  FILE *fin,*fout;
  if(argc!=3&&argc!=6&&argc!=5)
    {
      printf("Sai cu phap, Cu phap dung:\n1. tenct convertDB filein\n2. tenct filesplit filein numofcontact fileout1 fileout2\n3. tenct filemerge filein1 filein2 fileout\n");
      return FALSE;
    }
  if(strcmp(argv[1],"convertDB")==0)
    {
      changeDataBase(argv[2]);
    }
  if(strcmp(argv[1],"filesplit")==0)
  {
    filesplit(argv[2],atoi(argv[3]),argv[4],argv[5]);
  }
  if(strcmp(argv[1],"filemerge")==0)
    {
      filemerge(argv[2],argv[3],argv[4]);
    }
  if(strcmp(argv[1],"fileread")==0)
    {
      printContactFile(argv[2]);
    }
  return SUCCESS;
}
