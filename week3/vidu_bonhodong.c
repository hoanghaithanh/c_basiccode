#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(void)
{
  int i, n, *p;
  printf("Ban muon nhap bao nhieu so?\n");
  scanf("%d",&n);
  /*Cap phat bo nho cho P*/
  p = (int*) malloc(n*sizeof(int));
  if(p==NULL)
    {
      printf("Loi cap phat bo nho!\n");
      return 0;
    }
  /*Nhap n so nguyen*/
  for(i=1;i<=n;i++)
    {
      printf("Nhap so thu %d: ",i);
      scanf("%d",&p[i-1]);
    }
  for(i=n;i>=1;i--)
    {
      printf("So thu %d la: %d\n",i,p[i-1]);
    }
  /*giai phong bo nho*/
  free(p);
}
