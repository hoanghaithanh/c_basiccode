/*Nhap mot file dau vao, chuong trinh liet ke cac ky tu co trong file va so lan xuat hien cua cac ky tu do*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int argc, char* argv[])
{
	char ch;
	FILE *f;
	int char_table[94],i=0;

	/*Kiem tra dau vao*/
	if(argc!=2)
	{
		printf("Sai cu phap, cu phap dung: ten_chuong_trinh ten_file\n");
		return 0;
	}
	if((f=fopen(argv[1],"r"))==NULL)
	{
		printf("Loi doc file dau vao!\n");
		fclose(f);
		return 0;
	}

	/*Khoi tao mang char_table bang 0*/
	for(i=0;i<94;i++)
	{
	char_table[i]=0;
	}

	/*Lan luot quet tung ky tu va +1 o cac vi tri tuong ung cua chung trong mang*/
	do
	{
		ch = fgetc(f);
		if(ch<33 || ch>126) continue;
		char_table[((int)ch)-33]++;
	}
	while(ch!=EOF);
	
	/*Thay doi char_table de tinh chu HOA thanh chu thuong*/
	for(i=65;i<=90;i++)
	  {
	    char_table[i-65+97-33]+=char_table[i-33];
	    char_table[i-33]=0;
	  }

	/*In ket qua*/
	for(i=0;i<93;i++)
	{
		if(char_table[i]!=0)
		{
			printf("Co %d ky tu \'%c\' trong file.\n",char_table[i],i+33);
		}
	}	
	fclose(f);
	return 1;
}
