#include<stdio.h>
#include<stdlib.h>
#include"lib/fields.h"
#include<string.h>
#define SYNTAX_ERROR 0;
#define INPUT_ERROR -1;
int *line_diff;
int first_line_diff(IS is1,IS is2)
{
  int check1=0,check2=0,check_diff=0;
      check1 = get_line(is1);
      check2 = get_line(is2);
      if((check1==-1) && (check2==-1))
	{
	  return -1;
	}
      else
	  {
	    if(check1!=check2) return 1;
	  }

  do
    {
      check1 = get_line(is1);
      check2 = get_line(is2);
      if(strcmp(is1->text1,is2->text1)!=0||(check1!=check2)) return is1->line;
    }
  while(check1!=-1 && check2!=-1);
    return -1;
}

int diff_line_count(IS is1, IS is2, int **array)
{
  int check1 = 0, check2 = 0, numofdiffcount=1;

  *array = (int*) malloc(sizeof(int)*numofdiffcount);
  //printf("line1\n");
  (*array)[0] = (is1->line>is2->line ? is1->line:is2->line);
  // printf("%d\n",(*array)[0]);
  while(check1!=-1 && check2!=-1)
    {
      check1 = get_line(is1);
      check2 = get_line(is2);
      //printf("line2\n");
      if(check1==-1 && check2 != -1)
	{
	  while(check2!=-1)
	    {
	      numofdiffcount++;
	       // printf("line2.1\n");
	       (*array) = (int*) realloc((*array),numofdiffcount*sizeof(int));
	       //printf("line2.2\n");
	       (*array)[numofdiffcount-1] = is2->line;
	       //printf("%d\n",(*array)[numofdiffcount-1]);
	       // printf("line2.3\n");
	       check2 = get_line(is2);
	       //printf("line2.4\n");
	    }
	  //printf("line2.5\n");
	  return numofdiffcount;
	}
      //printf("line3\n");
      if(check1!=-1 && check2==-1)
		{
	  while(check1!=-1)
	    {
	      numofdiffcount++;
	      *array = (int*) realloc(*array,numofdiffcount*sizeof(int));
	      (*array)[numofdiffcount-1] = is1->line;
	      check1 = get_line(is1);
	    }
	  return is1->line;
	}
      //printf("line4\n");
      if(check1!=-1 && check2!=-1)
	{
	  if(strcmp(is1->text1,is2->text1)!=0)
	    {
	      numofdiffcount++;
	      *array = (int*) realloc(*array,numofdiffcount*sizeof(int));
	      (*array)[numofdiffcount-1] = is1->line;
	    }
	}
       
    }
  return numofdiffcount;
  
}

int main(int argc,char* argv[])
{
  /*Khoi tao bien vao*/
  IS is1, is2;
  int count=0,first_line = 0,i=0;
  

  /*Kiem tra dau vao*/
  if(argc!=3)
    {
      printf("Sai cu phap! Cu phap dung: ten_ct ten_file1 ten_file2\n");
      return SYNTAX_ERROR;
    }
  is1 = new_inputstruct(argv[1]);
  is2 = new_inputstruct(argv[2]);
  if(is1==NULL||is2==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return INPUT_ERROR;
    }

  /*so sanh hai file*/
  first_line = first_line_diff(is1,is2);
  if(first_line==-1)
    {
      printf("Hai file giong het nhau!\n");
      return 0;
    }
  else
    {
      printf("Dong dau tien khac nhau la dong %d\n",first_line);
    }

  printf("Cac dong khac nhau la:\n");

  count = diff_line_count(is1, is2, &line_diff);
  // printf("%d\n",count);

  for(i=0;i<count;i++)
    {
      printf("%d ",line_diff[i]);
    }
  printf("\n");
  jettison_inputstruct(is1); jettison_inputstruct(is2);
  return 1;
 
}
