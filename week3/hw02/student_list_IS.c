/*Viet chuong trinh doc mot file dau vao gom co 4 truong STT Hoten MSSV SDT, luu vao mot mang cau truc, sau do yeu cau nguoi dung nhap diem, luu ket qua ra mot file va them truong DIEM*/
/*Trong bai su dung thu vien fields voi cau truc IS de nhap du lieu dau vao*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"lib/fields.h"

typedef struct{
  int stt;
  char name[30];
  char MSSV[9];
  char SDT[12];
  float diem;
}student_List;
student_List* student_list;

int main(int argc,char* argv[])
{
  IS is;
  int numofstudent=0,i=0;
  char buff[30];
  FILE *f;

  /*Kiem tra dau vao*/
  if(argc!=2)
    {
      printf("Cu phap sai, cu phap dung: ten_ct ten_file\n");
      return 0;
    }
  if((is = new_inputstruct(argv[1]))==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return 0;
    }

  /*Nhap dau vao*/
  student_list = (student_List*)malloc(sizeof(student_List));
  while(get_line(is)!=-1)
    {
      numofstudent++;
      student_list = (student_List*)realloc(student_list,sizeof(student_List)*numofstudent);
      student_list[numofstudent-1].stt = atoi(is->fields[0]);
      strcpy(student_list[numofstudent-1].name,is->fields[1]);
      strcpy(student_list[numofstudent-1].MSSV,is->fields[2]);
      strcpy(student_list[numofstudent-1].SDT,is->fields[3]);

      printf("Nhap diem cua sinh vien thu %d ( %s ): ",numofstudent-1,student_list[numofstudent-1].name); //Nhap diem cua sinh vien chu numofstudent
      scanf("%f",&student_list[numofstudent-1].diem);
    }

  /*In ket qua ra man hinh*/
  for(i=0;i<numofstudent;i++)
    {
      printf("Hoc sinh thu %d\n",i+1);
      printf("%d\t%s\t%s\t%s\t%f\n",student_list[i].stt,student_list[i].name,student_list[i].MSSV,student_list[i].SDT,student_list[i].diem);
    }

  /*In ket qua ra file*/
  if((f=fopen("bangdiem.txt","w+"))==NULL)
    {
      printf("Loi ghi file!\n");
      return 0;
    }
  fprintf(f,"%s\t%s\t%s\t\t%s\t\t%s\n","STT","TEN","MSSV","SDT","DIEM");

  for(i=0;i<numofstudent;i++)
    {
      fprintf(f,"%d\t%s\t%s\t%s\t%0.2f\n",student_list[i].stt,student_list[i].name,student_list[i].MSSV,student_list[i].SDT,student_list[i].diem);
    }

  /*Giai phong cac thanh phan da khoi tao*/
  fclose(f);
  jettison_inputstruct(is);
  free(student_list);
  return 1;
}
