/*Viet chuong trinh doc mot file dau vao gom co 4 truong STT Hoten MSSV SDT, luu vao mot mang cau truc, sau do yeu cau nguoi dung nhap diem, luu ket qua ra mot file va them truong DIEM*/
/*Trong bai su dung ham fscanf de scan dau vao*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct{
  int stt;
  char name[30];
  char MSSV[9];
  char SDT[12];
  float diem;
}student_List;
student_List* student_list;

int main(int argc,char* argv[])
{
  /*Khoi tao bien*/
  int numofstudent=0,i=0,buff_stt;
  char buff_name[30],buff_MSSV[9],buff_SDT[12];
  FILE *f,*fin;

  /*Kiem tra dau vao*/
  if(argc!=2)
    {
      printf("Cu phap sai, cu phap dung: ten_ct ten_file\n");
      return 0;
    }
  if((fin=fopen(argv[1],"r"))==NULL)
    {
      printf("Loi doc file dau vao!\n");
      return 0;
    }
   
  /*Nhap dau vao*/
  student_list = (student_List*)malloc(sizeof(student_List));
printf("denday0\n");
  while(fscanf(fin,"%d\t%s\t%s\t%s\n",&buff_stt,buff_name,buff_MSSV,buff_SDT)!=EOF)
    {
      student_list = (student_List*)realloc(student_list,sizeof(student_List)*(numofstudent+1));
      student_list[numofstudent].stt = buff_stt;
      strcpy(student_list[numofstudent].name,buff_name);
      strcpy(student_list[numofstudent].MSSV,buff_MSSV);
      strcpy(student_list[numofstudent].SDT,buff_SDT);
      printf("Nhap diem cua sinh vien thu %d ( %s ): ",numofstudent+1,student_list[numofstudent].name); //Nhap diem sinh vien thu numofstudent+1
      scanf("%f",&student_list[numofstudent].diem);
      numofstudent++;
    }

  /*In ket qua ra man hinh*/
  for(i=0;i<numofstudent;i++)
    {
      printf("Hoc sinh thu %d\n",i+1);
      printf("%d\t%s\t%s\t%s\t%f\n",student_list[i].stt,student_list[i].name,student_list[i].MSSV,student_list[i].SDT,student_list[i].diem);
    }

  /*In ket qua ra file*/
  if((f=fopen("bangdiem.txt","w+"))==NULL)
    {
      printf("Loi ghi file!\n");
      return 0;
    }
  fprintf(f,"%s\t%s\t%s\t\t%s\t\t%s\n","STT","TEN","MSSV","SDT","DIEM");

  for(i=0;i<numofstudent;i++)
    {
      fprintf(f,"%d\t%s\t%s\t%s\t%0.2f\n",student_list[i].stt,student_list[i].name,student_list[i].MSSV,student_list[i].SDT,student_list[i].diem);
    }
  free(student_list);
  fclose(fin);fclose(f);
  return 1;
}
