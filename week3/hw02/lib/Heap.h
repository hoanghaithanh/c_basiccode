// ADT HEAP, HEAPIFY, BUILD A HEAP, HEAP SORT
// THIS IS MAX HEAP

/******
ATTENTION: FOR USING HEAP ADT, ARRAY INDEX MUST BE COUNTED FROM 1, NOT 0
******/

#ifndef HEAP_H
#define HEAP_H

#define MAX 100

// TYPEDEF DEFINE

typedef int ElementType;

// RETURN LEFT CHILD & RIGHT CHILD OF A BINARY TREE NODE IN ADT HEAP

int leftChild(int node)
{
	return node * 2;
}

int rightChild(int node)
{
	return node * 2 + 1;
}

// RETURN PARENT OF A NODE IN ADT HEAP

int parent(int node)
{
	return node / 2;
}

// EXTRACT HEAP'S MAX VALUE

int heapExtractMax(ElementType a[], int *n)
{
	ElementType maxValue;

	maxValue = a[1];
	a[1] = a[*n];
	(*n)--;
	maxHeapify(a, 1, *n);
	return maxValue;
}

// INCREASE A KEY IN A HEAP

void heapIncreaseValue(ElementType a[], int i, ElementType value)
{
	if(a[i] > value)
		printf("%s%d\n", "The value is lower than ", a[i]);
	else
	{
		a[i] = value;
		while( (i > 1) && (a[i] > a[parent(i)]) )
		{
			// Swap a[i] and a[parent(i)]
			ElementType temp = a[i];
			a[i] = a[parent(i)];
			a[parent(i)] = temp;

			i = parent(i);
		}
	}
}

// INSERT A NEW NODE INTO A HEAP

void heapInsert(ElementType a[], int *n, ElementType value)
{
	(*n)++;
	a[*n] = -32767;
	heapIncreaseValue(a, *n, value);

}

// MAX HEAPIFY

void maxHeapify(ElementType a[], int start, int end)
{
	if(start == end)
		return;

	// Compare the current node (a[start]) with its two children
	int largestPos;
	int r = rightChild(start);
	int l = leftChild(start);
	if( (l <= end) && (a[l]) > a[start])
		largestPos = l;
	else largestPos = start;
	if( (r <= end) && (a[r] > a[largestPos]) )
		largestPos = r;
	if(largestPos != start)
	{
		// a[start] will become child and a[largestPos] will become parent
		ElementType temp = a[start];
		a[start] = a[largestPos];
		a[largestPos] = temp;
		
		maxHeapify(a, largestPos, end);
	}
	else return;
}

// BUILD A MAX HEAP

void buildMaxHeap(ElementType a[], int n)
{
	int i;

	for(i = n / 2; i >= 1; i--)
		maxHeapify(a, i, n);
}

#endif