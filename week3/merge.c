#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int argc,char* argv[])
{
  int check1=0, check2=0,i=0;
  char buff1[100],buff2[100];
  FILE *f1, *f2, *f3;
  if(argc!=4)
    {
      printf("Loi cu phap, cu phap dung: ten_ct file_in_1 file_in_2 file_out_3\n");
      return 0;
    }

  if(((f1=fopen(argv[1],"r"))==NULL)||((f2=fopen(argv[2],"r"))==NULL))
    {
      printf("Loi doc file dau vao!\n");
      return 0;
    }
  if((f3=fopen(argv[3],"w+"))==NULL)
    {
      printf("Loi ghi file output!\n");
      return 0;
    }
  if( fgets(buff1, 100, f1)==NULL) check1 =1;
  if( fgets(buff2, 100, f2)==NULL) check2 =1;
  while((check1!=1)||(check2!=1))
    {
      if(check1!=1)
	{	
      for(i=0;i<strlen(buff1);i++)
	{
	  fputc(buff1[i],f3);
	}
      if( fgets(buff1, 100, f1)==NULL) check1 =1;
	}

       if(check2!=1)
	{	
      for(i=0;i<strlen(buff2);i++)
	{
	  fputc(buff2[i],f3);
	}
      if( fgets(buff2, 100, f2)==NULL) check2 =1;
	}
    }
  fclose(f1);fclose(f2);fclose(f3);
}
