#define MAX 100

// DEFINE STACK ADT

typedef char ElementType;

typedef struct
{
    ElementType stack[MAX];
    int top;
} StackType;

// INITIALIZE A STACK

void initializeStack(StackType *s)
{
    s->top = -1;
}

// CHECK WHETHER A STACK IS EMPTY

int isEmptyStack(StackType s)
{
    return (s.top == -1);
}

// CHECK WHETHER A STACK IS FULL

int isFullStack(StackType s)
{
    return (s.top == MAX - 1);
}

// PUSH STACK

void push(StackType *s, ElementType value)
{
    if (isFullStack(*s))
    {
        printf("%s\n", "Stack full, can't push.");
        return;
    }

    s->top++;
    s->stack[s->top] = value;
}

// POP STACK

int pop(StackType *s, ElementType *value)
{
    if (isEmptyStack(*s))
    {
        printf("%s\n", "Stack empty, can't pop.");
        return 0;
    }

    *value = s->stack[s->top];
    s->top--;

    return 1;
}

// PEEK STACK

ElementType peek(StackType s)
{
    return s.stack[s.top];
}
