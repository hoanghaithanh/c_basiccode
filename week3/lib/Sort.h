#ifndef SORT_H_
#define SORT_H_

typedef int ElementType;

void swap(ElementType *a, ElementType *b)
{
	ElementType temp = *a;
	*a = *b;
	*b = temp;
}

// SELECTION SORT

void selectionSort(ElementType a[], int n)
{
	int i, j, min;

	for (i = 0; i < n - 1; ++i)
	{
		min = i;
		for (j = i + 1; j <= n - 1; j++)
			if (a[j] < a[min])
				min = j;

		swap(&a[i], &a[min]);
	}
}

void printList(ElementType a[], int n)
{
	int i;

	for (i = 0; i < n; i++)
	{
		printf("%d\t", a[i]);
	}
}

// QUICK SORT

int partition(ElementType a[], int left, int right)
{
	int i, j, pivot;

	i = left;
	j = right + 1;
	pivot = a[left];

	while (i < j)
	{
		i++;
		while ((i <= right) && (a[i] < pivot))
			i++;
		j--;
		while ((j >= left) && (a[j] > pivot))
			j--;
		swap(&a[i], &a[j]);
	}

	swap(&a[i], &a[j]);
	swap(&a[j], &a[left]);

	return j;
}

void quickSort(ElementType a[], int left, int right)
{
	int pivot;

	if (left < right)
	{
		pivot = partition(a, left, right);
		if (left < pivot)
			quickSort(a, left, pivot - 1);
		if (right > pivot)
			quickSort(a, pivot + 1, right);
	}
}
#endif
